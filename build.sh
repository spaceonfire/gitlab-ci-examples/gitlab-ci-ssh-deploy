#!/usr/bin/env bash
set -e

PROJECT_NAME="$1"

mkdir -p "build/${PROJECT_NAME}"

ARGS=("$@")

for ((i = 1; i < $#; i++)); do
    ssh-keygen -b 2048 -t rsa -f "build/${PROJECT_NAME}/${ARGS[$i]}" -q -N ""
done
