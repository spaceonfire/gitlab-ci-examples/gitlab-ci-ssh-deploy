# Деплой по SSH используя GitLab CI

Шаблон конфигурации GitLab CI для деплоя по SSH с использованием утилиты Shipit.

Требуется:

- GitLab >=12.0 (multiple extends).
- GitLab Runner на Docker (с тегом `docker`)

## Подготовка

1. Создайте deploy token-ы для каждого окружения (`prod`, `stage`) с правами на чтение репозитория.
   Это можно сделать перейдя в `Settings -> Repository -> Deploy Tokens`.

1. Склонировать репозиторий на сервер используя созданные deploy token-ы:

   ```bash
   git clone https://<deploy-token-name>:<deploy-token>@<gitlab-url>/<repo-group>/<repo-name>.git
   ```

1. Если репозиторий был склонирован на сервер раньше, то необходимо настроить существующий remote:

   ```bash
   git remote set-url origin https://<deploy-token-name>:<deploy-token>@<gitlab-url>/<repo-group>/<repo-name>.git
   ```

1. При необходимости произведите любую необходимую настройку в зависимости от вашего проекта
   (конфиги, подключение к бд и прочее)

1. Создайте на сервере пользователя с ограниченными правами для деплоя

1. Настройте права доступа на директорию проекта. Рекомендуется использовать для этого ACL.

## Настройка GitLab CI

1. Склонируйте данный репозиторий себе на локальную машину.

1. Сгенерируйте SSH ключи для каждого окружения:

   ```bash
   # Схема
   bash ./build.sh <project-name> <env1> <env2> <envN>

   # Пример
   bash ./build.sh my-app stage prod
   ```

   Ключи будут сгенерированы в папке `build/<project-name>` для указанных окружений.

1. Пропишите публичные ключи в файле `~/.ssh/authorized_keys` на соответствующих серверах.

1. Перейдите в настройки `Settings -> CI/CD -> Environment variables` вашего репозитория и создайте
   следующие переменные:

   - `PROD_PRIVATE_KEY` - содержимое приватного ключа для окружения `prod`;
   - `PROD_REMOTE_IP` - IP адрес окружения `prod`.
     **Важно**: адрес должен быть доступен из сети вашего раннера;
   - `PROD_REMOTE_PATH` - путь до директории проекта на окружении `prod`;
   - `PROD_REMOTE_USER` - имя пользователя для деплоя на окружение `prod`;
   - `PROD_ENV_DOMAIN` - домен, по которому доступно окржение `prod`;
   - `STAGE_PRIVATE_KEY` - содержимое приватного ключа для окружения `stage`;
   - `STAGE_REMOTE_IP` - IP адрес окружения `stage`.
     **Важно**: адрес должен быть доступен из сети вашего раннера;
   - `STAGE_REMOTE_PATH` - путь до директории проекта на окружении `stage`;
   - `STAGE_REMOTE_USER` - имя пользователя для деплоя на окружение `stage`;
   - `STAGE_ENV_DOMAIN` - домен, по которому доступно окржение `stage`;

1. Скопируйте файлы `.gitlab-ci.yml` и `.shipit.tmpl` в корень вашего проекта и отредактируйте под
   ваши нужды

1. Добавьте в `.gitignore` строчку `.shipit`.

## Ссылки

- [Документация GitLab CI](https://docs.gitlab.com/ce/ci/yaml/README.html)
- [`sapegin/shipit`](https://github.com/sapegin/shipit)
- [Docker образ `spaceonfire/shipit`](https://github.com/dockeronfire/shipit)
